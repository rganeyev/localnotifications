﻿using System;
using System.Collections;
using System.Collections.Generic;
using LaikaBOSS.Configurator;
using UnityEngine;
using VoxelBusters.NativePlugins;

public class LocalNotifications : MonoBehaviour {
    private static CrossPlatformNotification CreateNotification(string text, long delaySeconds,
        eNotificationRepeatInterval repeatInterval) {
        // User info
        IDictionary userInfo = new Dictionary<string, string>();

        var iosProperties = new CrossPlatformNotification.iOSSpecificProperties {
            HasAction = true,
            AlertAction = null
        };


        var androidProperties = new CrossPlatformNotification.AndroidSpecificProperties {
            ContentTitle = Configurator.CurrentConfiguration.name,
            TickerText = text,
            LargeIcon = "icon.png",
        };
        //Keep the files in Assets/StreamingAssets/VoxelBusters/NativePlugins/Android folder.
        //place the custom sound file under Asset/VoxelBusters/NativePlugins/Plugins/NativeIOSCode/Common/Assets/Sound folder. 

        var notification = new CrossPlatformNotification {
            AlertBody = text,
            FireDate = DateTime.Now.AddSeconds(delaySeconds),
            RepeatInterval = repeatInterval,
            UserInfo = userInfo,
            iOSProperties = iosProperties,
            AndroidProperties = androidProperties,
            SoundName = "Notification.ogg"
        };


        return notification;
    }

    public static void CreateLocalNotification(string text, long delaySeconds,
        eNotificationRepeatInterval repeatInterval = eNotificationRepeatInterval.NONE) {
        var notification = CreateNotification(text, delaySeconds, repeatInterval);
        NPBinding.NotificationService.ScheduleLocalNotification(notification);
    }

    public static void CancelAllLocalNotifications() {
        NPBinding.NotificationService.CancelAllLocalNotification();
    }
}