﻿using UnityEngine;

public class ScheduleLocalNotifications : MonoBehaviour {
    public bool oneDayNotification;
    public bool threeDaysNotification;
    public bool sevenDaysNotification;

    private void OnApplicationPause(bool pauseStatus) {
        if (pauseStatus) Schedule();
        else CancelAll();
    }

    private void CancelAll() {
        LocalNotifications.CancelAllLocalNotifications();
    }

    private void OnApplicationQuit() {
        Schedule();
    }

    private void Start() {
        CancelAll();
    }

    private void Schedule() {
        CancelAll();
        if (oneDayNotification) {
            var text = Localization.Get("Push1TextKey");
            LocalNotifications.CreateLocalNotification(text, 60*60*24*1);
        }
        if (threeDaysNotification) {
            var text = Localization.Get("Push3TextKey");
            LocalNotifications.CreateLocalNotification(text, 60*60*24*3);
        }
        if (sevenDaysNotification) {
            var text = Localization.Get("Push7TextKey");
            LocalNotifications.CreateLocalNotification(text, 60*60*24*7);
        }
    }
}